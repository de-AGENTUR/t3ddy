<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "t3ddy".
 *
 * Auto generated 13-01-2022 16:34
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 't3ddy - Super cute tabs and accordions',
  'description' => 't3ddy provides comfortable tabs and accordion handling right in TYPO3 page module, using the techniques of gridelements.',
  'category' => 'be',
  'author' => 'Armin Ruediger Vieweg',
  'author_email' => 'armin@v.ieweg.de',
  'author_company' => '',
  'state' => 'stable',
  'uploadfolder' => false,
  'createDirs' => '',
  'clearCacheOnLoad' => 0,
  'version' => '1.3.0',
  'constraints' => 
  array (
    'depends' => 
    array (
      'typo3' => '9.5.0-10.4.99',
      'gridelements' => '3.0.5-11.9.99',
    ),
    'conflicts' => 
    array (
    ),
    'suggests' => 
    array (
    ),
  ),
  'autoload' => 
  array (
    'psr-4' => 
    array (
      'ArminVieweg\\T3ddy\\' => 'Classes',
    ),
  ),
  'clearcacheonload' => false,
);

