<?php
namespace ArminVieweg\T3ddy\Utilities;

/*  | This extension is part of the TYPO3 project. The TYPO3 project is
 *  | free software and is licensed under GNU General Public License.
 *  |
 *  | (c) 2014-2017 Armin Ruediger Vieweg <armin@v.ieweg.de>
 */

use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Database Utility
 *
 * @package ArminVieweg\T3ddy
 */
class DatabaseUtility
{
    /**
     * Returns a valid DatabaseConnection object that is connected and ready
     * to be used static
     *
     * @return ConnectionPool
     */
    public static function getDatabaseConnection()
    {
        return GeneralUtility::makeInstance(ConnectionPool::class);
    }
}
