<?php
return [

// File processing
    'T3ddy::buildCreateNewItemLink' => [
        'path' => '/t3ddy/createNewItemLink',
        'target' => \ArminVieweg\T3ddy\Ajax\LinkBuilder::class . '::createNewItemLink',
    ],
    'T3ddy::changeTabOrder' => [
        'path' => '/t3ddy/tabOrder',
        'target' => \ArminVieweg\T3ddy\Ajax\TabOrder::class . '::change',
    ]
];

